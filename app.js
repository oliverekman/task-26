// Class constructor

class CookieManager {
    constructor(name, data) {
      this.name = name;
      this.data = data;
    }
    
    // Create a New Cookie

    setCookie() {
      let d = new Date(Date.now() + 900000);
  
      document.cookie = this.name + '=' + this.data + ';' + 'expires=' + d.toUTCString();
    }

    // Get a Cookie
  
    getCookie() {
      const name = this.name + '=';
      let cookieDecode = decodeURIComponent(document.cookie);
      let ca = cookieDecode.split(';');
      for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }

    // Delete a Cookie
  
    deleteCookie() {
      return (document.cookie = this.name + '=' + this.data + ';' + 'max-age=0');
    }
  }


// Give the new Cookie a name and data  
  
let x = new CookieManager('Oliverio', 'King yes');


// Activate the methods

// x.setCookie();
// console.log(x.getCookie());
// x.deleteCookie();
